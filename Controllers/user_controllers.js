const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model')
const MAP_USER_REQ = require('./../helpers/map_user_req')

router.get('/',function(req,res,next) {
    UserModel
        .find()
        .sort({
            _id:-1
        })
        .then(user=>{
            return res.json(user)
        })
        .catch(err=>{
            return next({
                msg:"cannot find any user",
                status:404
            })
        })
})

router.post('/register', function (req,res,next) {
    // importing data requesting from body into Schema
    console.log('req.body is>>',req.body)
    const newUser = new UserModel({});

    const postUser = MAP_USER_REQ(newUser,req.body)

    postUser.save( function(err,saved) {
        if(err){
            return next(err);
        }else{
            return res.json(saved);
        }
    })
    // newUser.save()
    //     .then(user => {
    //         return res.json(user)
    //     })
    //     .catch(err => {
    //         return next(err)
    //     })
})

// find by id
router.route('/:userId')
    .get(function(req,res,next) {
        UserModel
            .findById(req.params.userId)
            .then(user=>{
                if(!user) {
                    return next({
                        msg:"Invalid userID",
                        status:404
                    })
                }
                return res.json(user)
            })
            .catch(err=>{
                return next(err)
            })
    })
    .put(function(req,res,next) {
        const newUser = new UserModel({});
        UserModel.findById(req.params.userId, function(err,user) {
            if(err) {
                return next(err)
            }
            if(!user) {
                return next({
                    msg:"Didn't found user to update",
                    status: 404
                })
            }
            const updatedMapUser = MAP_USER_REQ(newUser,req.body)

            newUser.save( function(err,updated) {
                if(err) {
                    return next(err);
                }
                res.json(updated)
            })
        })
    })
    .delete( function(req,res,next) {
        UserModel.findById(req.params.userId, function(err, user) {
            if(err) {
                return next(err)
            }
            if(!user) {
                return next({
                    msg:"User not found to delete",
                    status:404
                })
            }
            user.remove( function(err, removed) {
                if(err) {
                    return next(err)
                }
                return res.json(removed)
            })
        })
    })

module.exports = router;    