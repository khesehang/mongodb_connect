module.exports = function(user,userData) {
    if(userData.firstName)
        user.firstName = userData.firstName
    if(userData.lastName)
        user.lastName = userData.lastName
    if(userData.userName)
        user.userName = userData.userName
    if(userData.email)
        user.email = userData.email
    if(userData.password)
        user.password = userData.password
    if(userData.gender)
        user.gender = userData.gender
    if(userData.status)
        user.status =userData.status
    if(userData.role)
        user.role = userData.role
    if(userData.image)
        user.image = userData.image
        // CONTRACT SCHEMA
    if(!userData.contact)
        user.contact = {}
    if(userData.moblieNumber)
        user.contact.mobile = userData.mobileNumber
    if(userData.homeNumber)
        user.contact.home = userData.homeNumber
    if(userData.alternateNumber)
        user.contact.alternate = userData.alternateNumber
        // ADDRESS SCHEMA
    if(!userData.address)
        user.address = {}
    if(userData.temporary)
        user.address.temporary = userData.temporaryAddress
    if(userData.permanent)
        user.address.permanent = userData.permanentAddress
    
    return user;
}