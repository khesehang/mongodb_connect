const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    firstName:{
        type:String
    },
    lastName:String,
    userName:{
        type:String,
        required:true,
        unique:true,
        trim:true
    },
    email:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    gender:{
        type:String,
        enum:['male','female','other']
    },
    contact:{
        mobile:Number,
        home:Number,
        alternate:Number
    },
    status:{
        type:String,
        enum:['online','offline'],
        default:'offline'
    },
    role:{
        type:Number,//here 1 for admin, 2 for user, 3 for visitor
        default:2
    },
    country:{
        type:String,
        default:'Nepal'
    },
    address:{
        temporary:[String],
        permanent:String
    },
    image:String
},{
    timestamps: true, //this automatically show the update time
})

const UserModel = mongoose.model("user",UserSchema)
module.exports = UserModel;