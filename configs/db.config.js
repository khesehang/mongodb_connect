// here we gonna import all the things neede for using mongo so not to import manually in every file while we change file path
const mongodb = require('mongodb');
const MONGO_CLIENT =  mongodb.MongoClient;
const CONXN_URL = 'mongodb://localhost:27017'
const DB_NAME = 'learn'
const oid = mongodb.ObjectID;

module.exports = {
    MONGO_CLIENT: MONGO_CLIENT,
    CONXN_URL: CONXN_URL,
    DB_NAME: DB_NAME,
    OID: oid
}