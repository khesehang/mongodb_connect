const express = require('express');
const path = require('path');
const morgan = require('morgan');
const colors = require('colors')

const user_controll = require('./Controllers/user_controllers.js')

require('./db_initilize.js')

const app = express();
app.use(morgan('dev'))
// importing path from other files



app.use(express.urlencoded({
    extended: true
}))
app.use(express.json())

app.use('/user', user_controll)
app.use( function(error,req,res,next) {
    console.log("error is ",error)
    res.json({
        msg: error.msg || error,
        status: error.status || 400
    })
})

const PORT = 5000;
app.listen(PORT,
    console.log(`server running on port:${PORT}`.yellow.bold)
)