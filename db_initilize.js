const mongoose = require('mongoose');
const db_config = require('./configs/db.config')
const colors = require('colors')

// mongodb://localhost:27017/DB_NAME
mongoose.connect(db_config.CONXN_URL+ '/' + db_config.DB_NAME , {
    useNewUrlParser:true,
    useUnifiedTopology:true,
    useCreateIndex: true
}, function(err,done) {
    if(err){
        console.log('db connection failed',err)
    }else{
        console.log('db connection open'.red)
    }
})
// createConnection